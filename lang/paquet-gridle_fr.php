<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// N
	'gridle_nom' => 'NoiZetier : grille CSS Gridle',
	'gridle_slogan' => 'Une grille CSS au bon goût de noisette.',
	'gridle_description' => 'Ce plugin fournit la grille CSS Gridle. Il s\'interface avec le plugin « noiZetier : agencements » et permet donc d\'utiliser la grille avec les noisettes.',
);
