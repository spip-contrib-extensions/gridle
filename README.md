# Gridle

![Gridle](./gridle-xx.svg)

Ce plugin fournit la grille CSS [Gridle](https://github.com/Coffeekraken/gridle/).
Il déclare de plus la grille au plugin [Noizetier Layout](https://git.spip.net/spip-contrib-extensions/noizetier_layout), ce qui permet son utilisation dans les noisettes.

Les sources SCSS sont fournies, mais ne sont pas compatibles avec SCSSPHP.
Elles ne sont là que pour le développement, à compiler au moyen de Gulp.

La 1ère fois : `npm install`
Puis : `gulp watch` pour surveiller les changements et lancer la compilation automatiquement, ou `gulp css` pour une compilation ponctuelle.
