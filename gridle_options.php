<?php

// Sécurité
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

// Déclarer la grille
if (!defined('_NOIZETIER_GRILLE_CSS')) {
	define('_NOIZETIER_GRILLE_CSS', 'gridle');
}